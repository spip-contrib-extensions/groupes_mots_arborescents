<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// G
	'gma_description' => 'Les groupes de mots clés peuvent appartenir à un autre groupe de mots clés. Ils héritent alors des propriétés du groupe racine. Les sélecteurs de liens entre mots clés et les autres objets sont modifiés pour afficher les groupes racines avec l\'arborescence dedans.',
	'gma_nom' => 'Groupes arborescents de mots clés',
	'gma_slogan' => 'Permet de définir des sous groupes de mots clés',
);

?>