<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

if (!defined('_ECRIRE_INC_VERSION')) return;

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// E
	'erreur_parent_sur_groupe' => 'Le parent du groupe ne peut pas être ce groupe lui-même !',
	'erreur_parent_sur_groupe_enfant' => 'Le parent du groupe ne peut pas être un de ses enfants !',

	// G
	'gma_titre' => 'Groupes arborescents de mots clés',
	'groupe_parent' => 'Groupe parent',

	// I
	'icone_creation_sous_groupe_mots' => 'Créer un sous groupe de mots',

	// S
	'sous_groupe' => 'Sous groupe&nbsp;:',


);

?>